#!/usr/bin/env perl

use strict;
use warnings;
use diagnostics;
use File::Spec;
use File::Basename;

print "Configuring YTB ...\n";

sub browser {
	print "Browser: ";
	my $browser = <<>>; 
	if (! defined $browser) {print "bye\n"; exit;}
	chomp $browser;
	if (`uname` =~ /Darwin/) {
		while (1) {
			print "Is $browser an Application? [yes/no]";
			my $response = <<>>; 
			if (! defined $response) {
				print "bye\n";
				exit;
			}
			chomp $response;
			if (lc $response =~ /^no$/) {
				print "Treating '$browser' as a binary...\n";
				last;
			} elsif (lc $response =~ /^yes$/) {
				print "Treating '$browser' as Application...\n";
				if (
					! -d "/Applications/$browser" && 
					! -d "$ENV{q(home)}/Applications/$browser"
				) {
					print "$browser does not exist.";
					exit;
				}
				$browser = "open -a '$browser'";
				last;
			} else {
				print "invalid response.\n";
			}
		}
	} else {
		print "Treating '$browser' as binary...\n";
	}
	return $browser;
}

my $dir = dirname(__FILE__);

my $filename = File::Spec->catfile(dirname(__FILE__), "ytb.txt");

sub config {
	# open config.pm.
	my $browser = browser();
	open (my $fh, ">", File::Spec->catfile(dirname(__FILE__), "config.pm")) 
		or die "unable to open file. $!";

	print $fh <<EOF;
use strict;
use warnings;
use diagnostics;

package config;
use File::Basename;
use File::Spec qw(catfile);

our \$browser = "$browser";
our \$filename = catfile(dirname(__FILE__), "ytb.txt");

1;
EOF
}

sub gen_file {
	open (my $fh, ">", $filename) or die "Unable to open $filename $!";
	print $fh "";
}

if (! -f File::Spec->catfile(dirname(__FILE__), "config.pm")) {
	config();
}

if (! -f $filename) {
	gen_file();
}
