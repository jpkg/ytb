# ytb

This is a simple bookmarking script for Youtube Videos.

ytb stores all of the bookmarks in /ytb.txt, unless you configure it otherwise
in /config.pm.

## Installation

```sh
git clone https://gitlab.com/jpkg/ytb.git
# generates config.pm
./ytb/configure.pl
```

Add ytb to `$PATH` if you wish.
